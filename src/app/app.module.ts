import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { RESTAPIModule } from '@plone/restapi-angular';
import { TraversalModule } from 'angular-traversal';
import { CustomNavigationComponent } from './custom/custom-navigation/custom-navigation.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { APageViewComponent } from './custom/a-page-view/a-page-view.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomNavigationComponent,
    APageViewComponent,

  ],
  imports: [
    BrowserModule,
    RESTAPIModule,
    TraversalModule,
    NgbModule
  ],
  entryComponents: [APageViewComponent],
  //providers: [{ provide: 'CONFIGURATION', useValue: { BACKEND_URL: 'http://192.168.99.100:9002/Plone', } },],
  providers: [{ provide: 'CONFIGURATION', useValue: { BACKEND_URL: 'https://plonedemo.kitconcept.com/en/demo', } },],

  bootstrap: [AppComponent]
})
export class AppModule { }
