import { Component, OnInit } from '@angular/core';
import { ViewView, Services } from '@plone/restapi-angular';

@Component({
  selector: 'app-a-page-view',
  templateUrl: './a-page-view.component.html',
  styleUrls: ['./a-page-view.component.scss']
})
export class APageViewComponent extends ViewView {
  public context;
  constructor(public services: Services, ) { super(services); }

  ngOnInit() {
    this.getResponse();
  }
  getResponse() {
    this.services.api.get("/a-page").subscribe(res => {
      console.log(res);
      this.context = res;
    });
  }


}
