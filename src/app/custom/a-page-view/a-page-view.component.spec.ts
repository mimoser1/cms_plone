import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { APageViewComponent } from './a-page-view.component';

describe('APageViewComponent', () => {
  let component: APageViewComponent;
  let fixture: ComponentFixture<APageViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ APageViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(APageViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
