import { Component, OnInit } from '@angular/core';
import { GlobalNavigation, Services } from '@plone/restapi-angular';

@Component({
  selector: 'app-custom-navigation',
  templateUrl: './custom-navigation.component.html',
  styleUrls: ['./custom-navigation.component.scss']
})
export class CustomNavigationComponent extends GlobalNavigation implements OnInit {
  public collapse = true;
  constructor(public services: Services) { super(services); }

  ngOnInit() {
  }

}
