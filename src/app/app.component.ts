import { Component, OnInit } from '@angular/core';
import { PloneViews, ViewView, Services, AuthenticatedStatus, LoadingStatus } from '@plone/restapi-angular';
import { BehaviorSubject } from 'rxjs';
import { APageViewComponent } from './custom/a-page-view/a-page-view.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'plone';
  logged = false;
  loading = 'OK';
  error = '';
  public backendAvailable: BehaviorSubject<boolean>;
  constructor(public views: PloneViews, public services: Services) {

    this.views.initialize();

    //this.services.resource.defaultExpand.navigation = true;
    this.backendAvailable = this.services.api.backendAvailable;

    this.services.traverser.addView('view', 'Document', APageViewComponent);
  }

  ngOnInit() {
    this.services.authentication.isAuthenticated
      .subscribe((auth: AuthenticatedStatus) => {
        this.logged = auth.state;
      });

  }

}
